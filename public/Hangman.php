<!DOCTYPE html>
<?php
function redirectToIni()
{
	header("Location: index.php");
	exit();
}

if (!isset($_GET["correo"]))
	redirectToIni();

?>
<html lang="es-BO">
	<head>
	    <title>El ahorcado</title>
		<meta charset="UTF-8">
	    <link rel="stylesheet" type="text/css" href="css/main.css">
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.10/angular.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" media="screen">
	</head>
	<body ng-app="hangman" ng-controller="MainController">

	       <div class="page-header" align="center">
		    <h1>HANGMAN</h1>
		   </div>
	    <div class="img-thumbnail" alt="Imagen responsive">
		    <img class="health" ng-src="{{ 'img/' + health + '.png' }}" />
		</div>
		
		<div class="userInput" ng-hide="hasFinished()">

		    <div class="revealed">
			  {{ revealed }}
			</div>
			
			<form ng-submit="submitLetter()">
				<input type ="text" 
					   id="letterTextBox"
					   name="letterTextBox"
					   maxlength="1" 
					   size="1"
					   ng-model="letter"
					   autocomplete="off"
					   autofocus />
				<input class="btn btn-primary"
				       type ="submit" 
					   id="tryButton"
					   name="tryButton" 
					   value="Intentar" />
			</form>		  
		</div>
		
		<div ng-show="hasFinished()">
		    <div class="container">
			<div class="alert alert-success" ng-show="hasWon">Ganaste</div>
			<div class="alert alert-danger" ng-show="!hasWon">Perdiste</div>
			</div>
			<input class="btn btn-info" type ="submit" ng-click="init() " value="Volver a Jugar"/>
		</div>
				   <form action="index.php">
		   	    <br><input class="btn btn-danger" type="submit"  value="Salir" />
		   </form>
        <script>
		  var app = angular.module("hangman", []);
		   
		  app.controller("MainController", ["$scope", "$http", function($scope, $http) {
			$scope.letter = '';
			$scope.health = 6;
			$scope.revealed = "";
			$scope.hasWon = false;
			
			$scope.hasFinished = function() { 
			   return $scope.health == 0 || $scope.hasWon;
			};
			
			$scope.init = function() {
			   $http.post('play.php').then( processResponse );
			};
			
			$scope.submitLetter = function() {
			   var data = { letter: $scope.letter };
			   $http.post('play.php', data).then(processResponse);
			   $scope.letter = '';
			};
			
			$scope.init();
			
			function processResponse(response) {
			  $scope.hasWon = response.data.hasWon;
			  $scope.health = response.data.health;
			  $scope.revealed = response.data.revealed;
			}
		  }]);
		  
		  
		</script>		
	</body>
</html>



